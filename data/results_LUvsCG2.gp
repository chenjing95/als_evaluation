reset
set term postscript eps color solid 20
set output "results_LUvsCG2.eps"
set grid
set key top left
set xlabel "#latent feature"
#set yrange [0:*]
set ylabel "RMSE"


set style line 1  lt 1 lw 4 pt 8 ps 2 lc rgb "#D55E00"
set style line 2  lt 2 lw 4 pt 6 ps 2 lc rgb "#56B4E9"


plot "results_LUvsCG2.dat" using 2:xtic(1) title 'LU' with linespoints ls 1, '' using 3:xtic(1) title 'CG' with linespoints ls 2

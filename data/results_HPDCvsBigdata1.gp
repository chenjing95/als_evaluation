reset
set term postscript eps color solid 20
set output "results_HPDCvsBigdata1.eps"
set grid
set key top left
set xlabel "#latent feature"
set yrange [0:*]
set ylabel "Execution time (s)"


set style line 1  lt 1 lw 4 pt 8 ps 2 lc rgb "#D55E00"
set style line 2  lt 2 lw 4 pt 6 ps 2 lc rgb "#56B4E9"
set style line 3  lt 3 lw 4 pt 4 ps 2 lc rgb "#CC79A7"
set style line 4  lt 4 lw 4 pt 12 ps 2 lc rgb "#009E73"
set style line 5  lt 5 lw 4 pt 2 ps 2 lc rgb "#999999"
set style line 6  lt 6 lw 4 pt 10 ps 2 lc rgb "#D54E00"


plot "results_HPDCvsBigdata11.dat" using 2:xtic(1) title 'CuMF_ML1M' with linespoints ls 1, '' using 3:xtic(1) title 'Bigdata_ML1M' with linespoints ls 2, '' using 4:xtic(1) title 'CuMF_YHR4' with linespoints ls 3, '' using 5:xtic(1) title 'Bigdata_YHR4' with linespoints ls 4, '' using 6:xtic(1) title 'CuMF_LAFM' with linespoints ls 5, '' using 7:xtic(1) title 'Bigdata_LAFM' with linespoints ls 6

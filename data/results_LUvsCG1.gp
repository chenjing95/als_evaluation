reset
set term postscript eps color solid 20
set output "results_LUvsCG1.eps"
set grid
set key top left
set xlabel "#latent feature"
set yrange [0:*]
set ylabel "Execution time [s]"


set style line 1  lt 1 lw 4 pt 8 ps 2 lc rgb "#D55E00"
set style line 2  lt 2 lw 4 pt 6 ps 2 lc rgb "#56B4E9"
set style line 3  lt 3 lw 4 pt 4 ps 2 lc rgb "#CC79A7"
set style line 4  lt 4 lw 4 pt 12 ps 2 lc rgb "#009E73"


plot "results_LUvsCG1.dat" using 2:xtic(1) title 'NTFX_LU' with linespoints ls 1, '' using 3:xtic(1) title 'NTFX_CG' with linespoints ls 2, '' using 4:xtic(1) title 'YHR1_LU' with linespoints ls 3, '' using 5:xtic(1) title 'YHR1_CG' with linespoints ls 4

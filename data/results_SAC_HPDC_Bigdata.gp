reset
set terminal postscript eps color solid enhanced 18
#set terminal postscript eps monochrome 18
set output "results_SAC_HPDC_Bigdata.eps"
set key default
set key out horiz
set key top center

set grid y
set style line 1  lt -1 lw 4 pt 2 ps 1
set style line 2  lt -1 lw 4 pt 4 ps 1
set style line 3  lt -1 lw 4 pt 8 ps 1
set style line 4  lt 0 lw 4 pt 4 ps 1
set style line 5  lt 0 lw 4 pt 8 ps 1
set boxwidth 1 absolute
set style data histogram
set style histogram cluster gap 2
set style fill solid
#set yrange[0:1000]
#set ytics 200
set ylabel "Execution time [s,log scale]"
set logscale y 2
#set y2label "Speedup (x)"
set xtic rotate by -45 scale 0
#set xlabel "instruction mix"


set ytics nomirror
#set y2tics
set tics out
#set autoscale y2
#set y2range [0:*]

plot 'results_SAC_HPDC_Bigdata.dat' using 2:xtic(1) title "SAC" fill pattern 4 lc rgb "#D55E00", '' using 3 title "Bigdata" fill pattern 5 lc rgb "#56B4E9", '' using 4 title "CuMF" fill pattern 1 lc rgb "#009E73"



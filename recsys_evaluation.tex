\section{Performance Evaluation} \label{sec_recsys_evaluation}
\subsection{Comparing Convergence Speed}
In this section, we evaluate the convergence speed of the ALS implementations in \texttt{SAC}, \texttt{Bigdata}, and \texttt{CuMF} on various datasets. 
Figure~\ref{fig_results_SAC_HPDC_Bigdata} shows the execution time for these implementations with feature space size $f=10$. 
We see that \texttt{Bigdata} performs the best on all datasets, with a speedup ranging from 25.7$\times$ to 78.0$\times$ compared to \texttt{SAC} and a speedup of upto 15.8$\times$ compared to \texttt{CuMF}. 
Also, we notice that \texttt{Bigdata} runs particularly fast on the small datasets such as YHR4, LAFM, and ML1M.
As for \texttt{SAC}, one GPU thread is used to update a row of the $X$ matrix, where all the temporary data of $Y^{T}Y$ is allocated dynamically in the kernel function. 
But when $f$ becomes large, there is insufficient global memory space remained for dynamic allocation and thus the kernel failed to run. 
In this case, this implementation does not scale over the latent factor. 
Thereafter, we focus on comparing \texttt{CuMF} and \texttt{Bigdata} when $f$ is large.

%3.5 - 15.8 times parallel speedup when compared with CuMF. Especially compared with the other implementations, Bigdata achieves maximum speedup on small datasets(i.e., YHR4, LAFM, ML1M). 

\begin{figure}[!h]
\centering
\includegraphics[width=0.70\textwidth]{./data/results_SAC_HPDC_Bigdata.eps}
\caption{A convergence speed comparison of three implementations. We use the same configuration of 5 iterations and $f$=10. It uses 8192 blocks and 32 threads per block in SAC. All the datasets exploit one batch in CuMF, which using $m \times f/2$ threads totally. In Bigdata, the 3D grid block size is (batch\_size,$f$/$nb$,$f$/$nb$) and threads number per block is ($dx$,$dy$), where batch\_size=4096, $kb$=5, $nb$=5 and $dx$=$dy$=5.}
\label{fig_results_SAC_HPDC_Bigdata}
\end{figure}

%As a result of the implementation of SAC has no scalability in terms of latent feature $f$, so we just compare others. In the implementation of SAC, a thread is used to update a row of $X$ matrix and all the results of $Y^{T}Y$ save in global memory without free action in each iteration, which results in the memory overflow. 

%In this section, we exploit six datasets and divide them into two segment according to their data size, Figure~\ref{fig_results_HPDCvsBigdata1} represents small datasets while Figure~\ref{fig_results_HPDCvsBigdata2} stands for big datasets. The threads configurations in the implementation of HPDC use batch\_size blocks and $f$/2 threads per block. The parameters of implementation of Bigdata are listed in Table~\ref{tbl_parametersofBigdata}. Although the parameters are derived from several tests, we can not guarantee they are optimal parameters for the sparse-syrk kernels. 

Figure~\ref{fig_results_HPDCvsBigdata} presents the performance comparison between \texttt{CuMF} and \texttt{Bigdata}. 
\texttt{CuMF} uses $batch\_size$ thread blocks each with $f$/2 threads when $f\in[10, 90]$,
 whereas each thread block has 64 threads when $f$=100. 
Conjugate gradient method (CG) is used in matrix factorization of \texttt{CuMF}. 
The tuning parameters of \texttt{Bigdata} are listed in Table~\ref{tbl_parametersofBigdata}. 
We observe that, \texttt{Bigdata} generally runs faster than \texttt{CuMF} over latent factors ranging from 10 to 90 on all datasets. 
Besides, \texttt{CuMF} scales nearly linearly as $f$ changes from 10 to 90.  
Thanks to the customized kernel when $f$=100, the execution time of \texttt{CuMF} is less than \texttt{Bigdata} in some datasets and even less than that with a smaller $f$. 

To summarize, \texttt{Bigdata} not only leverages the hierarchical thread organization of modern GPU architectures as \texttt{CuMF} does, 
but also exploits a batched strategy in detail to maximize the usage of the on-chip fast memory (Figure~\ref{fig_Bigdata_schematic}). 
In \texttt{CuMF}, each thread block is used for updating a row/column in kernels, while \texttt{Bigdata} uses multiple thread blocks per row, where the number of thread blocks relates to tile sizes and latent factors.
Therefore, the configurations of the batched \texttt{Bigdata} are vital to the achievement of the best performance (Section 4.3).

\begin{figure}[!h]
\centering

\subfigure[small datasets]{
%\begin{minipage}[t]{0.48\linewidth}
%\centering
\includegraphics[width=0.46\textwidth]{./data/results_HPDCvsBigdata1.eps}
%\end{minipage}
\label{fig_results_HPDCvsBigdata1}
}
\subfigure[big datasets]{
%\begin{minipage}[t]{0.48\linewidth}
%\centering
\includegraphics[width=0.46\textwidth]{./data/results_HPDCvsBigdata2.eps}
%\end{minipage}
\label{fig_results_HPDCvsBigdata2}
}
\caption{The trend of convergence speed over latent feature $f$ on various datasets where iteration times=5. All datasets are divided into two sections, the left represents small datasets while the right stands for big datasets.} 
\label{fig_results_HPDCvsBigdata}
\end{figure}

\begin{table}[!h]
\caption{Configurations of Bigdata}
\label{tbl_parametersofBigdata}
\begin{center}
\scalebox{1.0}{
\begin{tabular}{|l|c|c|c|c|c|c|c|c|c|c|} \hline
& $f$=10 & $f$=20 & $f$=30 & $f$=40 & $f$=50 & $f$=60 & $f$=70 & $f$=80 & $f$=90 & $f$=100\\ \hline
$nb$ & 5 & 10 & 15 & 20 & 25 & 30 & 35 & 40 & 30 & 50  \\ \hline
$kb$ & 5 & 5 & 5 & 10 & 5 & 15 & 7 & 20 & 10 & 25  \\ \hline
$dx$ & 5 & 5 & 5 & 10 & 5 & 15 & 7 & 10 & 10 & 25  \\ \hline
$dy$ & 5 & 5 & 5 & 10 & 5 & 15 & 7 & 10 & 10 & 25  \\ \hline
\end{tabular}}
\end{center}
\end{table}

\subsection{Comparing LU and CG for \texttt{CuMF}}
After obtaining the product of $Y^{T}Y$, we need to factor the $f\times f$ matrix into two triangular matrice in Equation~\ref{eq:x_u} of Section~\ref{sec_als_algorithm}. 
\texttt{CuMF} offers two solvers for matrix factorization: the direct LU solver and the conjugate gradient (CG) solver. 
LU decomposition represents a $direct$ procedure for decomposing an $f \times f$ matrix $A$ into a product of a lower triangular matrix $L$ and an upper triangular matrix $U$, satisfying $LU=A$. 
Conjugate gradient method (CG) is an algorithm for the numerical solution of particular systems of linear equations, 
namely those whose matrix is symmetric and positive-definite. 
It is often implemented as an $iterative$ algorithm, applicable to sparse systems that are too large to be handled by a direct implementation such as the Cholesky decomposition. 
Figure~\ref{fig_LUvsCG} shows the performance comparison of LU and CG. 
We see that CG runs consistently faster than LU over $f$, while achieving almost the same \textit{root mean square error} (RMSE). Thus, we choose to use CG throughout the context (Figure~\ref{fig_results_HPDCvsBigdata}). 
%Obviously, the precision(RMSE) of LU and CG is approximately same, but the convergence speed of CuMF with CG is more efficient than LU.   

%In CuMF, the two solutions are used to solve matrix factorization. 

\begin{figure}[!h]
\centering

\subfigure[speed]{
\includegraphics[width=0.46\textwidth]{./data/results_LUvsCG1.eps}
\label{fig_LUvsCG1}
}
\subfigure[RMSE]{
\includegraphics[width=0.46\textwidth]{./data/results_LUvsCG2.eps}
\label{fig_LUvsCG2}
}
\caption{The performance comparison of LU and CG decomposotion in CuMF.} 
\label{fig_LUvsCG}
\end{figure}

\subsection{Tuning Parameters for \texttt{Bigdata}}
For \texttt{Bigdata}, selecting suitable configuration parameters is key to achieving high performance. 
Figure~\ref{fig_parameter_bigdata} shows how the performance changes over the block configurations when $f$=90. 
We observe that the performance changes between these configurations is dramatic, i.e., with the execution time changes of $2.1\times$. 
Particularly, the configurations of ($nb$=30, $kb$=10, $dx$=$dy$=10) are the optimal parameters for most of datasets, 
%in the sparse-syrk GPU kernel of these configurations 
and \texttt{Bigdata} performs better with the configurations compared with others. 
This is  because it can fully occupy and exploit the most hierarchical resources of GPU (i.e., shared memory and registers). 
However, there is no fixed optimal configuration(s) for all feature space sizes and/or datasets. 
Besides, there exits a difference in terms of select a right configuration parameter between the updating $X$ over $Y$ kernel and updating $Y$ over $X$ kernel.   

\begin{figure}[!t]
\centering
\includegraphics[width=0.80\textwidth]{./data/results_parameter.eps}
\caption{The performance changes over the configurations on various datasets, where $f$=90. The format is (nb,kb,dx,dy).}
\label{fig_parameter_bigdata}
\end{figure}

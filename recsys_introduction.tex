\section{Introduction} \label{sec_recsys_introduction}

% matrix factorization and recommender systems
In a recommender system, we aim to build a model by training with observed incomplete rating data (i.e., a user's preference over all items) and then predict his/her preference over items not rated~\cite{DBLP:journals/computer/KorenBV09}. Among the recommendation approaches, \textit{matrix factorization} was empirically shown to be a better solution than traditional nearest-neighbour approaches in the Netflix Prize competition~\cite{DBLP:conf/icdm/YuHSD12}. Since then, there has been a large amount of work dedicated to the design of fast and scalable methods for large-scale matrix factorization problems~\cite{DBLP:conf/aaim/ZhouWSP08, DBLP:journals/computer/KorenBV09, DBLP:journals/jmlr/TakacsPNT09}. 

% Alternating least square (ALS), and parallelization
Among the matrix factorization techniques, \textit{alternating least squares} (ALS) has been proved to be an effective one~\cite{DBLP:journals/computer/KorenBV09}. 
Compared to \textit{stochastic gradient descent} (SGD)~\cite{DBLP:conf/kdd/GemullaNHS11, DBLP:conf/icdm/TeflioudiMG12}, the ALS algorithm is not only inherently parallel, but can incorporate implicit ratings~\cite{DBLP:journals/computer/KorenBV09}. 
Nevertheless, the ALS algorithm involves parallel sparse matrix manipulation~\cite{DBLP:phd/phdthesisliu} which is challenging to achieve high performance due to imbalanced workload~\cite{DBLP:journals/IPDPS/Liu201547,DBLP:conf/ICS/2015}, random memory access~\cite{Wang:2016:PTS:2925426.2926291} and task dependency~\cite{Liu:A16}. 
This particularly holds when parallelizing and optimizing ALS on modern GPUs. 


To address this issue, researchers have investigated various solutions. 
In~\cite{DBLP:conf/sac/RodriguesJD15}, Rodrigues et al. present a CUDA-based ALS implementation on GPU (\texttt{SAC}), which is claimed to run faster than the implementation on a multi-core CPU. 
In~\cite{DBLP:conf/hpdc/TanCF16}, Tan et al. provides a CUDA-based matrix factorization library (\texttt{CuMF}). 
It uses various techniques to maximize the performance on multiple GPUs and achieves a speedup of 6-10 times when compared with the state-of-art distributed CPU solutions. 
In~\cite{DBLP:conf/bigdataconf/GatesAKD15}, Mark et al. accelarate ALS on the basis of implicit feedback datasets (\texttt{Bigdata}), which proven to be 10 times faster than the implementations available in the GraphLab and Spark MLlib and additional 2 times faster than their CPU implementation. 

% Contributions
%In this paper, we diagnose these above ALS implementations and then propose their remaining problem in parallelizing ALS respectively. On the other hand, we evaluate the performance of these solutions on various datasets and analyze the experimental data. On the basis of the evaluation, we come up with a new method to optimize sparse, irregular and intensive memory access and build mathematic model to check its feasibility.

In this work, we evaluate the three GPU implementations (\texttt{SAC}, \texttt{CuMF}, and \texttt{Bigdata}) of ALS-based matrix factorization for recommender systems. 
The evaluation work is performed not only at the algorithmic level and but with empirical performance results. 
We observe that \texttt{Bigdata} outperforms the other two implementations for the majority of cases. 
This is fundamentally due to the fact that \texttt{Bigdata} efficiently maps the hierarchical GPU threads and judiciously organizes the rating data with on-chip fast memories.
We then answer the question of \textit{"whether there remains optimization room on GPU?"}. 
An observation shows that we can further improve the factorization performance by exploiting data reuses between updating distinct vectors. 
Also, we build an analytical performance model to calculate the potential benefits and overheads. 

To summarize, we make the following contributions. 
\begin{itemize}
%\item We analyze each solution's various techniques implemented to maximize the ALS performance on either single or multiple GPUs.

%\item We diagnose advantages and disadvantages of these solutions, which could be used for reference in our later research.

\item We analyze and compare three typical GPU implementations of ALS-based matrix factorization for recommender systems (Section~\ref{sec_recsys_background}).

\item We evaluate the ALS solvers in terms of convergence speed and the performance impact of tuning parameters on various datasets (Section~\ref{sec_recsys_evaluation}).
% influence of methods in matrix factorization and the effect of parameters.

\item We propose an approach to exploit data locality in ALS and build an analytical model to predict the potential performance benefits (Section~\ref{sec_recsys_model}).
%\item We propose a new method to deal with intensive memory access and build methematic model to check its feasibility.
\end{itemize} 

% paper organization 
%The remainder of this paper is organized as follows. Section~\ref{sec_recsys_background} describes ALS algrithm and diagnoses three proposed solutions(SAC~\cite{DBLP:conf/sac/RodriguesJD15},CuMF~\cite{DBLP:conf/hpdc/TanCF16}, and Bigdata~\cite{DBLP:conf/bigdataconf/GatesAKD15}) and we present setup including hardware and software and datasets in Section~\ref{sec_recsys_setup} and evaluate them in Section~\ref{sec_recsys_evaluation}. Section~\ref{sec_recsys_model} proposes our method and the process of building model. Section~\ref{sec_related_work} lists the related work and Section~\ref{sec_conclusion} concludes our work.
